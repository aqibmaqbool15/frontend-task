$(document).ready(function () {
   $(".toggle-button-container").on("click", function(){
        $(".header-right").toggleClass("left");
   });
});

wow = new WOW(
   {
       boxClass: 'wow',
       animateClass: 'animated',
       offset: 0,
       mobile: true,
       live: true
   }
)
wow.init();